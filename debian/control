Source: fim
Section: graphics
Priority: optional
Maintainer: Michele Martone <michelemartone@users.sourceforge.net>
Uploaders: Rafael Laboissière <rafael@debian.org>
Build-Depends: autoconf (>= 2.71),
               bison,
               debhelper-compat (= 13),
               flex,
               groff,
               inkscape,
               libaa1-dev,
               libcaca-dev,
               libdjvulibre-dev,
               libexif-dev,
               libgif-dev,
               libgtk-3-dev,
               libjpeg-dev,
               libpng-dev,
               libreadline-dev,
               libsdl1.2-dev,
               libtiff-dev,
               libfl-dev,
               pkgconf
Standards-Version: 4.7.2
Homepage: https://www.nongnu.org/fbi-improved/
Vcs-Git: https://salsa.debian.org/debian/fim.git
Vcs-Browser: https://salsa.debian.org/debian/fim
Rules-Requires-Root: no

Package: fim
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: inkscape
Description: scriptable frame buffer, X.org and ascii art image viewer
 FIM is a highly customizable scriptable image viewer targeted at the
 users who are comfortable with software like the Vim text editor or the Mutt
 mail user agent. FIM aims to be a "swiss army knife" for viewing images.
 Its code derives from the "Fbi" framebuffer image viewer by Gerd Hoffmann.
 FIM is multidevice: it has X support via the SDL library and ascii art output
 via the aalib library.
 .
 It supports image description files, file search and filtering using regular
 expressions on filenames and descriptions, caption display, customizable
 status line, EXIF tags display, EXIF-based image rotation, recursive directory
 traversal, reading from stdin, and can e.g. jump between two images
 remembering scale and position.
 .
 It can speed up loading by image caching and speed up scaling with mipmaps.
 It offers GNU readline command line autocompletion and history,
 completely customizable key bindings, external/internal (if-while based)
 scriptability (through return codes, standard input/output, and commands given
 at invocation time, an initialization file, Vim-like autocommands), and much
 more.
